// Kelas Majalah yang mengimplementasikan DapatDiterbitkan
class Magazine implements DapatDiterbitkan {
    private String judul;
    private int tahunTerbit;

    public Magazine(String judul, int tahunTerbit) {
        this.judul = judul;
        this.tahunTerbit = tahunTerbit;
    }

    @Override
    public void cetakInformasi() {
        System.out.println("Majalah: " + judul + " (Tahun Terbit: " + tahunTerbit + ")");
    }
}