// Kelas Buku yang mengimplementasikan DapatDiterbitkan
class Book implements DapatDiterbitkan {
    private String judul;
    private String penulis;

    public Book(String judul, String penulis) {
        this.judul = judul;
        this.penulis = penulis;
    }

    @Override
    public void cetakInformasi() {
        System.out.println("Buku: " + judul + " (Penulis: " + penulis + ")");
    }
}
