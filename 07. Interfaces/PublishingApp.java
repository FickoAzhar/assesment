public class PublishingApp {
    public static void main(String[] args) {
        // Membuat objek Buku dan Majalah
        DapatDiterbitkan buku = new Book("Harry Potter", "J.K. Rowling");
        DapatDiterbitkan majalah = new Magazine("National Geographic", 2022);

        // Menerbitkan informasi objek
        buku.cetakInformasi();
        majalah.cetakInformasi();
    }
}