public class CompanyDemo {
    public static void main(String[] args) {
        // Membuat objek Company
        Company company = new Company("ABC Corporation");

        // Membuat departemen dan karyawan
        Department hrDepartment = new Department("HR Department");
        Employee employee1 = new Employee("Alice", 101);
        Employee employee2 = new Employee("Bob", 102);

        // Menambahkan karyawan ke departemen
        hrDepartment.addEmployee(employee1);
        hrDepartment.addEmployee(employee2);

        // Menambahkan departemen ke perusahaan
        company.addDepartment(hrDepartment);

        // Menampilkan informasi perusahaan, departemen, dan karyawan
        System.out.println("Company Name: " + company.getCompanyName());
        System.out.println("Departments: ");
        for (Department department : company.getDepartments()) {
            System.out.println(" - " + department.getName());
            System.out.println("   Employees: ");
            for (Employee employee : department.getEmployees()) {
                System.out.println("   - " + employee.getName() + " (ID: " + employee.getEmployeeId() + ")");
            }
        }
    }
}