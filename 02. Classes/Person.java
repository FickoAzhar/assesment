//Class Person
class Person {
    String name;
    int age;

    public void introduction() {
        System.out.println("Halo, nama saya " + name);
        System.out.println("umur saya adalah " + age + " tahun.");
    }

    public static void main(String[] args) {
        Person person = new Person();
        person.name = "Ficko Azhar";
        person.age = 22;

        person.introduction();
        
    }
}

