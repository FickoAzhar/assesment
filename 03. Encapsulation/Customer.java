public class Customer {
    private String nama;
    private String alamat;
    private String email;
    private String nomorTelepon;

    public Customer(String nama, String alamat, String email, String nomorTelepon) {
        this.nama = nama;
        this.alamat = alamat;
        this.email = email;
        this.nomorTelepon = nomorTelepon;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomorTelepon() {
        return nomorTelepon;
    }

    public void setNomorTelepon(String nomorTelepon) {
        this.nomorTelepon = nomorTelepon;
    }

    public void tampilkanInfo() {
        System.out.println("Informasi Pelanggan:");
        System.out.println("Nama: " + nama);
        System.out.println("Alamat: " + alamat);
        System.out.println("Email: " + email);
        System.out.println("Nomor Telepon: " + nomorTelepon);
    }

    public static void main(String[] args) {
        // Membuat objek Customer
        Customer customer1 = new Customer("Ficko Azhar", "Jl. Raya No. 123", "fick@gmail.com", "123-456-789");
        Customer customer2 = new Customer("dudung", "Jl. Mawar No. 45", "dudung@gmail.com", "987-654-321");

        // Menampilkan informasi pelanggan
        customer1.tampilkanInfo();
        System.out.println("\n");
        customer2.tampilkanInfo();
    }
}
