// Kelas dasar (superclass)
class BangunDatar {
    double panjang;
    double lebar;

    BangunDatar(double panjang, double lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    double hitungLuas() {
        return panjang * lebar;
    }
}