public class InheritanceExample {
    public static void main(String[] args) {
        // Membuat objek PersegiPanjang (subclass)
        PersegiPanjang persegiPanjang = new PersegiPanjang(5.0, 3.0);

        // Menghitung luas menggunakan metode dari superclass
        double luas = persegiPanjang.hitungLuas();

        System.out.println("Luas Persegi Panjang: " + luas);
    }
}
