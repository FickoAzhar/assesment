// Kelas turunan (subclass) pertama
class Gambar extends Media {
    @Override
    void main() {
        System.out.println("Tampilkan gambar.");
    }
}