public class PolymorphismExample {
    public static void main(String[] args) {
        Media media1 = new Gambar();
        Media media2 = new Audio();

        media1.main(); // Memanggil metode dari Gambar
        media2.main(); // Memanggil metode dari Audio
    }
}