// Kelas turunan (subclass) kedua
class Audio extends Media {
    @Override
    void main() {
        System.out.println("Putar audio.");
    }
}