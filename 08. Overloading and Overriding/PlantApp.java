public class PlantApp {
    public static void main(String[] args) {
        // Create a FruitPlant object
        FruitPlant plant1 = new FruitPlant("Orange Tree", 5, "Orange");

        // Display plant information with and without age
        System.out.println("Plant Information:");
        plant1.displayInfo();
        System.out.println("\nPlant Information Without Age:");
        plant1.displayInfo(true);
    }
}