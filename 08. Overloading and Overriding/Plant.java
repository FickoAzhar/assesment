// Abstract class (superclass)
abstract class Plant {
    private String name;
    private String type;
    private int age;

    public Plant(String name, String type, int age) {
        this.name = name;
        this.type = type;
        this.age = age;
    }

    // Abstract method to be overridden by subclasses
    abstract void displayInfo();

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getAge() {
        return age;
    }
}