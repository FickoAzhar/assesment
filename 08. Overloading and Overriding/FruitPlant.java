// First subclass: Fruit Plant
class FruitPlant extends Plant {
    private String favoriteFruit;

    public FruitPlant(String name, int age, String favoriteFruit) {
        super(name, "Fruit Plant", age);
        this.favoriteFruit = favoriteFruit;
    }

    @Override
    void displayInfo() {
        System.out.println("Name: " + getName());
        System.out.println("Type: " + getType());
        System.out.println("Age: " + getAge() + " years");
        System.out.println("Favorite Fruit: " + favoriteFruit);
    }

    // Overloaded method to display info without age
    void displayInfo(boolean withoutAge) {
        System.out.println("Name: " + getName());
        System.out.println("Type: " + getType());
        if (!withoutAge) {
            System.out.println("Age: " + getAge() + " years");
        }
        System.out.println("Favorite Fruit: " + favoriteFruit);
    }
}