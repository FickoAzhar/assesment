public class AbstractionLibrary {
    public static void main(String[] args) {
        // Membuat objek Buku dan Majalah
        Buku buku1 = new Buku("Harry Potter", 2001, "J.K. Rowling");
        Majalah majalah1 = new Majalah("National Geographic", 2022, "National Geographic Society");

        // Menampilkan informasi item perpustakaan
        buku1.tampilkanInfo();
        majalah1.tampilkanInfo();
    }
}