// Kelas abstrak (superclass)
abstract class ItemPerpustakaan {
    private String judul;
    private int tahunTerbit;

    public ItemPerpustakaan(String judul, int tahunTerbit) {
        this.judul = judul;
        this.tahunTerbit = tahunTerbit;
    }

    // Metode abstrak yang akan di-override oleh subclass
    abstract void tampilkanInfo();

    public String getJudul() {
        return judul;
    }

    public int getTahunTerbit() {
        return tahunTerbit;
    }
}