// Kelas turunan (subclass) pertama: Buku
class Buku extends ItemPerpustakaan {
    private String pengarang;

    public Buku(String judul, int tahunTerbit, String pengarang) {
        super(judul, tahunTerbit);
        this.pengarang = pengarang;
    }

    @Override
    void tampilkanInfo() {
        System.out.println("Buku - Judul: " + getJudul() + ", Pengarang: " + pengarang + ", Tahun Terbit: " + getTahunTerbit());
    }
}