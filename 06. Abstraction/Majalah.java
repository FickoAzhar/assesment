// Kelas turunan (subclass) kedua: Majalah
class Majalah extends ItemPerpustakaan {
    private String penerbit;

    public Majalah(String judul, int tahunTerbit, String penerbit) {
        super(judul, tahunTerbit);
        this.penerbit = penerbit;
    }

    @Override
    void tampilkanInfo() {
        System.out.println("Majalah - Judul: " + getJudul() + ", Penerbit: " + penerbit + ", Tahun Terbit: " + getTahunTerbit());
    }
}